# Real Time Scheduling in C# Console

Project that illustrates Real Time Scheduling using Rate-Monotonic (RM) and Earlier Deadline First (EDF) methodologies in C#.

# How? 

1. Clone this project. 
2. Run `dotnet run` command in the projects root using any terminal. 
3. OR, Hit the debug button on VSCode. 