using System;
namespace real_time_scheduling_in_c_sharp
{
  class GeneralTest
  {
    public void PerformTest()
    {
      Test1();
      Test2();
      Test3();
      Test4();
      Test5();
    }
    public void Test1()
    {
      SchedulingTask task1 = new SchedulingTask("T1", 1, 4);
      SchedulingTask task2 = new SchedulingTask("T2", 2, 5);
      SchedulingTask task3 = new SchedulingTask("T3", 5, 20);

      SchedulingTask[] tasks = { task1, task2, task3 };

      RateMonotonicScheduling rms = new RateMonotonicScheduling(tasks, "Test1");
      if (rms.IsSchedulable())
      {
        rms.Schedule();
      }

      EarlierDeadlineFirst edf = new EarlierDeadlineFirst(tasks, "Test1");
      if (edf.IsSchedulable())
      {
        edf.Schedule();
      }
    }

    public void Test2()
    {
      SchedulingTask task1 = new SchedulingTask("T1", 2, 5);
      SchedulingTask task2 = new SchedulingTask("T2", 4, 7);

      SchedulingTask[] tasks = { task1, task2 };

      RateMonotonicScheduling rms = new RateMonotonicScheduling(tasks, "Test2");
      if (rms.IsSchedulable())
      {
        rms.Schedule();
      }

      EarlierDeadlineFirst edf = new EarlierDeadlineFirst(tasks, "Test2");
      if (edf.IsSchedulable())
      {
        edf.Schedule();
      }
    }

    public void Test3()
    {
      SchedulingTask task1 = new SchedulingTask("T1", 2, 4);
      SchedulingTask task2 = new SchedulingTask("T2", 3, 8);
      SchedulingTask task3 = new SchedulingTask("T3", 2, 16);

      SchedulingTask[] tasks = { task1, task2, task3 };

      RateMonotonicScheduling rms = new RateMonotonicScheduling(tasks, "Test3");
      if (rms.IsSchedulable())
      {
        rms.Schedule();
      }

      EarlierDeadlineFirst edf = new EarlierDeadlineFirst(tasks, "Test3");
      if (edf.IsSchedulable())
      {
        edf.Schedule();
      }
    }

    public void Test4()
    {
      SchedulingTask task1 = new SchedulingTask("T1", 3, 20);
      SchedulingTask task2 = new SchedulingTask("T2", 2, 5);
      SchedulingTask task3 = new SchedulingTask("T3", 2, 10);

      SchedulingTask[] tasks = { task1, task2, task3 };

      RateMonotonicScheduling rms = new RateMonotonicScheduling(tasks, "Test4");
      if (rms.IsSchedulable())
      {
        rms.Schedule();
      }

      EarlierDeadlineFirst edf = new EarlierDeadlineFirst(tasks, "Test4");
      if (edf.IsSchedulable())
      {
        edf.Schedule();
      }
    }

    public void Test5()
    {
      SchedulingTask task1 = new SchedulingTask("T1", 2, 10);
      SchedulingTask task2 = new SchedulingTask("T2", 1, 5);
      SchedulingTask task3 = new SchedulingTask("T3", 5, 30);
      SchedulingTask task4 = new SchedulingTask("T4", 2, 15);

      SchedulingTask[] tasks = { task1, task2, task3, task4 };

      RateMonotonicScheduling rms = new RateMonotonicScheduling(tasks, "Test5");
      if (rms.IsSchedulable())
      {
        rms.Schedule();
      }

      EarlierDeadlineFirst edf = new EarlierDeadlineFirst(tasks, "Test5");
      if (edf.IsSchedulable())
      {
        edf.Schedule();
      }
    }
  }
}