using System;
using System.Linq;

namespace real_time_scheduling_in_c_sharp
{
  class RateMonotonicScheduling : BaseScheduling
  {
    public RateMonotonicScheduling(SchedulingTask[] taskList, string testName = "TEST") : base(taskList, testName, "RateMonotonicScheduling")
    {
    }

    public override SchedulingTask GetNextTask(int currentTime)
    {
      return this.TaskList.Where(task =>
           task.ScheduledTasks
           .Where(period => period.StartTime <= currentTime && period.Remaining > 0)
           .ToArray().Length > 0)
           .OrderBy(task => task.Priority)
           .FirstOrDefault();
    }

  }

}