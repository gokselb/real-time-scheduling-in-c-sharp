using System;
using System.Linq;

namespace real_time_scheduling_in_c_sharp
{
  class EarlierDeadlineFirst : BaseScheduling
  {
    public EarlierDeadlineFirst(SchedulingTask[] taskList, string testName = "TEST") : base(taskList, testName, "EarlierDeadlineFirst")
    {
    }

    public override SchedulingTask GetNextTask(int currentTime)
    {
      return this.TaskList.Where(task =>
         task.ScheduledTasks
         .Where(period => period.StartTime <= currentTime && period.Remaining > 0)
         .ToArray().Length > 0)
         .OrderBy(task =>
            task.ScheduledTasks
                .Where(period => period.StartTime <= currentTime && period.Remaining > 0)
                .OrderBy(period => period.Deadline)
                .FirstOrDefault()
                .Deadline
         )
         .ThenBy(task =>
           task.ScheduledTasks
               .Where(period => period.StartTime <= currentTime && period.Remaining > 0)
               .OrderBy(period => period.Deadline)
               .FirstOrDefault()
               .StartTime
         )
         .FirstOrDefault();
    }

  }

}