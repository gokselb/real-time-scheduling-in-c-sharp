using System;
using System.Linq;

namespace real_time_scheduling_in_c_sharp
{
  abstract class BaseScheduling
  {
    public SchedulingTask[] TaskList;
    public long LCM;

    public string MethodologyName;
    public BaseScheduling(SchedulingTask[] taskList, string testName = "TEST", string methodologyName = "")
    {
      this.MethodologyName = methodologyName;
      this.TaskList = taskList;

      this.PrintHeader(testName);

      this.FindLCM();

      // Order by priority which is Period in RM case.
      this.AssingPriorities();
      this.PrintOrder();
      this.ScheduleAll();
    }

    public void Schedule()
    {
      Utils.PrintToConsole("");
      Utils.PrintToConsole("Scheduling Result");
      Utils.PrintToConsole("");
      string textToPrint = "";
      SchedulingTask currentTask;
      currentTask = this.TaskList[0];
      for (int currentTime = 0; currentTime < this.LCM; currentTime++)
      {

        currentTask = GetNextTask(currentTime);

        textToPrint += this.printCurrentTask(currentTask);

        if (currentTask != null)
        {
          currentTask.ScheduledTasks.Where(period => period.Remaining > 0).FirstOrDefault().Remaining--;
        }
      }

      textToPrint += "|";
      Utils.PrintToConsole(textToPrint);
      this.PrintTotalPeriod();

    }

    public abstract SchedulingTask GetNextTask(int currentTime);

    public bool IsSchedulable()
    {
      double numberOfTask = this.TaskList.Length;

      if (numberOfTask == 0)
      {
        return false;
      }

      double maxValue = numberOfTask * (Math.Pow(2, (1 / numberOfTask)) - 1);
      double total = 0;
      foreach (var task in this.TaskList)
      {
        total += ((double)task.Capacity / (double)task.Period);
      }



      Utils.PrintToConsole($" Max Schedulable Utilization Value is {maxValue:0.0000} ");
      Utils.PrintToConsole($" Utilization is {total:0.0000} ");
      Utils.PrintToConsole($" Is Schedulable? {total:0.0000} < {maxValue:0.0000} ==> {(total < maxValue)} ");
      Utils.PrintToConsole($" Is it possible to Schedule? {total:0.0000} <= 1 ==> {total <= 1} ");

      return total <= 1;
    }

    private void ScheduleAll()
    {
      for (int i = 0; i < this.TaskList.Length; i++)
      {
        SchedulingTask task = TaskList[i];
        long totalNumberOfRuns = this.LCM / task.Period;
        ScheduledTasks[] listOfScheduledTasks = new ScheduledTasks[totalNumberOfRuns];
        for (int j = 0; j < totalNumberOfRuns; j++)
        {
          listOfScheduledTasks[j] = new ScheduledTasks(
            j * task.Period,
            task.Capacity,
            (j + 1) * task.Period
          );
        }
        task.ScheduledTasks = listOfScheduledTasks;
      }
    }

    private string printCurrentTask(SchedulingTask task)
    {
      return $"|{task?.Name ?? "  "} ";
    }
    private void AssingPriorities()
    {
      this.TaskList = this.TaskList.OrderBy(task => task.Period).ToArray();

      for (int i = 0; i < this.TaskList.Length; i++)
      {
        this.TaskList[i].Priority = i + 1;
      }
    }
    private void FindLCM()
    {
      this.LCM = Utils.FindLCM(this.TaskList.Select(val => val.Period).ToArray());
      Utils.PrintToConsole(" LCM is " + this.LCM.ToString());

    }

    private void PrintOrder()
    {
      string text = "";
      text += " Priority is =>  ";
      foreach (var task in this.TaskList)
      {
        text += task.Name;
        if (Array.FindIndex(this.TaskList, search => search.Name == task.Name) != (this.TaskList.Length - 1))
        {
          text += " > ";
        }
      }
      Utils.PrintToConsole(text);
    }
    private void PrintTotalPeriod()
    {
      string textToPrint = "";
      for (int i = 0; i < this.LCM; i++)
      {
        textToPrint += "|---";

      }
      textToPrint += "|";
      Utils.PrintToConsole(textToPrint);
      textToPrint = "";
      for (int i = 0; i < this.LCM + 1; i++)
      {
        textToPrint += $"{i}  ";
        if (i < 10)
        {
          textToPrint += " ";
        }
      }
      Utils.PrintToConsole(textToPrint);
      this.PrintFooter();
    }
    private void PrintHeader(string testName)
    {
      for (int i = 0; i < Console.WindowWidth; i++)
      {
        Console.Write("#");
      }
      // Console.WriteLine("\n RateMonotonicScheduling");
      Utils.PrintToConsole($"{MethodologyName} --- {testName}");
      for (int i = 0; i < Console.WindowWidth; i++)
      {
        Console.Write("#");
      }
      Utils.PrintToConsole("");


    }
    private void PrintFooter()
    {
      Utils.PrintToConsole("");
      for (int i = 0; i < Console.WindowWidth; i++)
      {
        Console.Write("#");
      }
      Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine();
    }

  }

}