namespace real_time_scheduling_in_c_sharp
{
  class SchedulingTask
  {
    public string Name;
    public int Capacity;
    public int Period;
    public int Priority;

    public ScheduledTasks[] ScheduledTasks;


    public SchedulingTask(string name, int capacity, int period)
    {
      this.Name = name;
      this.Capacity = capacity;
      this.Period = period;
    }
  }

  class ScheduledTasks
  {
    public int StartTime;
    public int Remaining;
    public int Deadline;

    public ScheduledTasks(int startTime, int remaining, int deadline)
    {
      StartTime = startTime;
      Remaining = remaining;
      Deadline = deadline;
    }
  }
}